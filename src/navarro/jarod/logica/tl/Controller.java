package navarro.jarod.logica.tl;
import navarro.jarod.logica.bl.*;
import navarro.jarod.logica.dl.*;

import java.util.ArrayList;

public class Controller {
    CL capa = new CL();

    public void registrarCarrera(String codigo, String nombre){
        Carrera tmpcarrera = new Carrera(codigo,nombre);
        capa.registrarCarrera(tmpcarrera);
    }


    public void registrarCurso(String codigo, String nombre, int creditos){
        Curso tmpcurso = new Curso(codigo,nombre,creditos);
        capa.registrarCurso(tmpcurso);
    }

    public boolean carreraExiste(String codigo){
        return capa.carreraExiste(codigo);
    }

    public boolean cursoExiste(String codigo){
        return capa.cursoExiste(codigo);
    }

    public boolean cursoExisteEnCarrera(String codigoCarrera, String codigoCurso){
        return capa.cursoExisteEnCarrera(codigoCarrera,codigoCurso);
    }

    public String[] listarCursos(){
        return capa.listarCursos();
    }

    public String[] listarCarreras(){
        return capa.listarCarreras();
    }

    public void asignarCursoACarrera(String codigoCarrera,String codigoCurso){
        capa.asignarCursoACarrera(codigoCarrera,codigoCurso);
    }

    public void registrarGrupo(String codigoCurso, int numero){
        Grupo tmpgrupo = new Grupo(numero);
        capa.registrarGrupo(codigoCurso,tmpgrupo);
    }

    public boolean grupoExiste(int numero){
        return capa.grupoExiste(numero);
    }


    public void registrarProfesor(int numeroGrupo,String id, String nombre, String apellido, String profesion, String correo){
        Profesor tmpprofesor = new Profesor(id,nombre,apellido,profesion,correo);
        capa.registrarProfesor(numeroGrupo,tmpprofesor);
    }

    public boolean profesorExiste(String id){
        return capa.profesorExiste(id);
    }


    public void asignarEstudianteAGrupo(int numeroGrupo,String id, String nombre, String apellidos, String correo, String direccion, int edad){
        Estudiante tmpestudiante = new Estudiante(id,nombre,apellidos,correo,direccion,edad);
        capa.asignarEstudiante(numeroGrupo,tmpestudiante);
    }

    public String ejemploContains(String nombre){
        Curso tmpCurso = new Curso();
        tmpCurso.setNombre(nombre);
        return capa.ejemploContains(tmpCurso);
    }

}
