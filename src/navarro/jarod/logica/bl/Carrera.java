package navarro.jarod.logica.bl;

import org.omg.CORBA.NO_IMPLEMENT;

import java.util.ArrayList;

public class Carrera {
    private String codigo;
    private String nombre;
    private ArrayList<Curso> cursos = new ArrayList<>();

    public Carrera() {
        codigo = " ";
        nombre = " ";
    }

    public Carrera(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(Curso tmpcurso) {
        cursos.add(tmpcurso);
    }

    @Override
    public String toString() {
        return
                "Codigo: " + codigo +
                        ", nombre: " + nombre +
                        ", cursos: " + cursos;
    }
}
