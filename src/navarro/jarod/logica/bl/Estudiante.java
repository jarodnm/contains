package navarro.jarod.logica.bl;

public class Estudiante {
    private String id;
    private String nombre;
    private String apellidos;
    private String correo;
    private String direccion;
    private int edad;

    public Estudiante() {
        id = " ";
        nombre = " ";
        apellidos = " ";
        correo = " ";
        direccion = " ";
        edad = 0;
    }

    public Estudiante(String id, String nombre, String apellidos, String correo, String direccion, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correo = correo;
        this.direccion = direccion;
        this.edad = edad;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return
                "id: " + id +
                        ", nombre: " + nombre +
                        ", apellidos: " + apellidos +
                        ", correo: " + correo +
                        ", direccion: " + direccion +
                        ", edad: " + edad;
    }
}
