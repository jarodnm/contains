package navarro.jarod.logica.bl;

import java.util.ArrayList;

public class Grupo {
    private int numero;
    private ArrayList<Estudiante> estudiantes = new ArrayList<>();
    private Profesor profesorAsignado;

    public Grupo() {
        numero = 0;
        profesorAsignado = new Profesor();
    }

    public Grupo(int numero) {
        this.numero = numero;
        this.profesorAsignado = new Profesor();
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public ArrayList<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(Estudiante tmpestudiante) {
        estudiantes.add(tmpestudiante);
    }

    public Profesor getProfesorAsignado() {
        return profesorAsignado;
    }

    public void setProfesorAsignado(Profesor profesorAsignado) {
        this.profesorAsignado = profesorAsignado;
    }

    @Override
    public String toString() {
        return
                "Numero: " + numero +
                        ", estudiantes: " + estudiantes +
                        ", profesor asignado: " + profesorAsignado;
    }
}
