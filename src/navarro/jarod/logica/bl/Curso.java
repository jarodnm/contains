package navarro.jarod.logica.bl;

import java.util.Objects;

public class Curso {
    private String codigo;
    private String nombre;
    private int creditos;
    private Grupo grupoAsociado;

    public Curso() {
    codigo = " ";
    nombre = " ";
    creditos = 0;
    grupoAsociado = new Grupo();
    }

    public Curso(String codigo, String nombre, int creditos) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.creditos = creditos;
        this.grupoAsociado = new Grupo();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    public Grupo getGrupoAsociado() {
        return grupoAsociado;
    }

    public void setGrupoAsociado(Grupo grupoAsociado) {
        this.grupoAsociado = grupoAsociado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Curso curso = (Curso) o;
        return Objects.equals(nombre, curso.nombre);
    }

    @Override
    public String toString() {
        return
                "codigo: " + codigo +
                        ", nombre: " + nombre +
                        ", creditos: " + creditos +
                        ", grupo asociado: " + grupoAsociado ;
    }
}
