package navarro.jarod.logica.dl;
import java.util.ArrayList;
import navarro.jarod.logica.bl.*;

import javax.swing.*;

public class CL {
    static ArrayList<Carrera> carreras = new ArrayList<>();
    static ArrayList<Curso> cursos = new ArrayList<>();

    public void registrarCarrera(Carrera tmpcarrera){
        carreras.add(tmpcarrera);
    }

    public void registrarCurso(Curso tmpcurso){
        cursos.add(tmpcurso);
    }

    public boolean carreraExiste(String codigo){
        boolean existe = false;
        for(Carrera dato: carreras){
            if(dato.getCodigo().equals(codigo)){
                existe = true;
            }
        }
        return existe;
    }

    public boolean cursoExiste(String codigo){
        boolean existe = false;
        for(Curso dato: cursos){
            if(dato.getCodigo().equals(codigo)){
                existe = true;
            }
        }
        return existe;
    }

    public void asignarCursoACarrera(String codigoCarrera, String codigoCurso){
        for(Carrera dato : carreras){
            for(Curso data:cursos){
                if(dato.getCodigo().equals(codigoCarrera)&&data.getCodigo().equals(codigoCurso)){
                    dato.setCursos(data);
                }
            }
        }

    }

    public String[] listarCursos(){
        String[] datos =  new String[cursos.size()];
        int pos = 0;
        for(Curso dato : cursos){
            datos[pos] = dato.toString();
            pos++;
        }
        return datos;
    }

    public String[] listarCarreras(){
        String [] datos = new String[carreras.size()];
        int pos = 0;
        for(Carrera dato : carreras){
            datos[pos] = dato.toString();
            pos++;
        }
        return datos ;
    }

    public boolean cursoExisteEnCarrera(String codigoCarrera, String codigoCurso){
        boolean existe = false;
        for(Carrera dato: carreras){
            for(Curso data : dato.getCursos()){
                if(dato.getCodigo().equals(codigoCarrera)&&data.getCodigo().equals(codigoCurso)){
                    existe = true;
                }
            }
        }
        return existe;
    }

    public void registrarGrupo(String codigoCurso, Grupo tmpgrupo){
        for(Curso dato : cursos){
            if(dato.getCodigo().equals(codigoCurso)){
                dato.setGrupoAsociado(tmpgrupo);
            }
        }
    }

    public boolean grupoExiste(int numero){
        boolean existe = false;
        for(Curso dato : cursos){
            if(dato.getGrupoAsociado().getNumero() == numero){
                existe = true;
            }
        }
        return existe;
    }

    public void registrarProfesor(int numeroGrupo, Profesor tmpprofesor){
        for(Curso dato : cursos){
            if(dato.getGrupoAsociado().getNumero() == numeroGrupo){
                dato.getGrupoAsociado().setProfesorAsignado(tmpprofesor);
            }
        }
    }

    public boolean profesorExiste(String id){
     boolean existe = false;
        for(Curso dato : cursos){
            if(dato.getGrupoAsociado().getProfesorAsignado().getId().equals(id)){
              existe = true;
            }
        }
        return existe;
    }

    public void asignarEstudiante(int numeroGrupo,Estudiante tmpestudiante){
        for(Curso dato : cursos){
            if(dato.getGrupoAsociado().getNumero() == numeroGrupo){
                dato.getGrupoAsociado().setEstudiantes(tmpestudiante);
            }
        }
    }

    public String ejemploContains(Curso tmpcurso){
        String resultado = "No está en el array list";
        for(Curso dato : cursos){
            if(cursos.contains(tmpcurso)){
                resultado = "Si está en el array list";
                break;
            }
        }
        return resultado;
    }
}
