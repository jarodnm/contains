package navarro.jarod.logica;


import java.io.*;

import navarro.jarod.logica.tl.*;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        int opcion;
        do {
            menu();
            out.println("Digite la opción que guste");
            opcion = Integer.parseInt(in.readLine());
            noSalir = procesarOpcion(opcion);
        } while (noSalir);
    }

    public static void menu() {
        out.println("1.Listar Carreras");
        out.println("2.Listar Cursos");
        out.println("3.Registrar Carrera");
        out.println("4.Registrar Curso");
        out.println("5.Asociar curso a carrera");
        out.println("6.Registrar Grupo");
        out.println("7.Registrar Profesor");
        out.println("8.Registrar Estudiantes en grupo");
        out.println("9.Contains");
        out.println("10.Salir");
    }

    public static boolean procesarOpcion(int opcion) throws IOException {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                listarCarreras();
                break;
            case 2:
                listarCursos();
                break;
            case 3:
                registrarCarrera();
                break;
            case 4:
                registrarCurso();
                break;
            case 5:
                asignarCurso();
                break;
            case 6:
                registrarGrupo();
                break;
            case 7:
                registrarProfesor();
                break;
            case 8:
                registrarEstudiantes();
                break;
            case 9:
                ejemploContains();
                break;
            case 10 :
                noSalir = false;
                out.println("Gracias por usar el programa!");
                break;
            default:
                out.println("Digite una opción valida");
                break;
        }
        return noSalir;
    }

    public static void registrarCarrera() throws IOException {
        String codigo;
        String nombre;
        out.println("Digite el codigo de la carrera");
        codigo = in.readLine();
        if (!gestor.carreraExiste(codigo)) {
            out.println("Digite el nombre de la carrera");
            nombre = in.readLine();
            gestor.registrarCarrera(codigo, nombre);
        } else {
            out.println("La carrera ya fue registrada previamente!");
        }
    }

    public static void registrarCurso() throws IOException {
        String codigo;
        String nombre;
        int creditos;

        out.println("Digite el codigo del curso");
        codigo = in.readLine();
        if (!gestor.cursoExiste(codigo)) {
            out.println("Digite el nombre del curso");
            nombre = in.readLine();
            out.println("Digite los creditos del curso");
            creditos = Integer.parseInt(in.readLine());
            gestor.registrarCurso(codigo, nombre, creditos);
        } else {
            out.println("El curso ya existe!");
        }

    }

    public static void asignarCurso() throws IOException {
        String codigoCarrera;
        String codigoCurso;
        out.println("Digite el codigo de la carrera");
        codigoCarrera = in.readLine();
        if (gestor.carreraExiste(codigoCarrera)) {
            out.println("Digite el codigo del curso");
            codigoCurso = in.readLine();
            if (gestor.cursoExiste(codigoCurso)) {
                if (!gestor.cursoExisteEnCarrera(codigoCarrera, codigoCurso)) {
                    gestor.asignarCursoACarrera(codigoCarrera, codigoCurso);
                } else {
                    out.println("La carrera ya contiene ese curso");
                }
            } else {
                out.println("El curso no existe!");
            }
        } else {
            out.println("La carrera no existe!");

        }
    }

    public static void registrarGrupo() throws IOException {
        String codigo;
        int numero;
        out.println("Digite el codigo del curso");
        codigo = in.readLine();
        if (gestor.cursoExiste(codigo)) {
            out.println("Digite el numero del grupo");
            numero = Integer.parseInt(in.readLine());
            if (!gestor.grupoExiste(numero)) {
                gestor.registrarGrupo(codigo, numero);
            } else {
                out.println("El grupo ya existe!");
            }
        } else {
            out.println("El curso no existe");
        }

    }


    public static void registrarProfesor() throws IOException {
        int numeroGrupo;
        String id;
        String nombre;
        String apellido;
        String profesion;
        String correo;
        out.println("Digite el numero del grupo");
        numeroGrupo = Integer.parseInt(in.readLine());
        if (gestor.grupoExiste(numeroGrupo)) {
            out.println("Digite el id del profesor");
            id = in.readLine();
            if (!gestor.profesorExiste(id)) {
                out.println("Digite el nombre");
                nombre = in.readLine();
                out.println("Digite el apellido");
                apellido = in.readLine();
                out.println("Digite la presión del profesor");
                profesion = in.readLine();
                out.println("Digite el correo del profesor");
                correo = in.readLine();
                gestor.registrarProfesor(numeroGrupo, id, nombre, apellido, profesion, correo);
            }{
                out.println("El profesor ya fue registrado previamente");
            }
        }else{
            out.println("El grupo no existe");
        }
    }

    public static void registrarEstudiantes()throws IOException{
        int numeroGrupo;
        String id;
        String nombre;
        String apellidos;
        String correo;
        String direccion;
        int edad;
        String des;
        out.println("Digite el numero del grupo");
        numeroGrupo = Integer.parseInt(in.readLine());
        if(gestor.grupoExiste(numeroGrupo)){
            do {
                out.println("Digite el id del estudiante");
                id = in.readLine();
                out.println("Digite el nombre del estudiante");
                nombre = in.readLine();
                out.println("Digite los apellidos");
                apellidos = in.readLine();
                out.println("Digite el correo");
                correo = in.readLine();
                out.println("Digite la direccion");
                direccion = in.readLine();
                out.println("Digite la edad");
                edad = Integer.parseInt(in.readLine());
                gestor.asignarEstudianteAGrupo(numeroGrupo, id, nombre, apellidos, correo, direccion, edad);
                out.println("Digite 1 para registrar otro estudiante en este grupo o cualquier otra cosa para salirse");
                des = in.readLine();

            }while (des.equals("1"));

        }
    }

    public static void listarCursos() throws IOException {
        for (String dato : gestor.listarCursos()) {
            out.println(dato);
        }
    }

    public static void listarCarreras() throws IOException {
        for (String dato : gestor.listarCarreras()) {
            out.println(dato);
        }
    }

    public static void ejemploContains()throws IOException{
        String nombreCurso = "Ciencias";
        String resultado;
        resultado = gestor.ejemploContains(nombreCurso);
        out.println(resultado);
    }


}
